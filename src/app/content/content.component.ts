import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss'],
  host: {'[class.content]': 'true'}
})
export class ContentComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
