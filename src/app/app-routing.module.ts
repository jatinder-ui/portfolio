import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CoverComponent } from './nav/cover/cover.component';
import { ContentComponent } from './content/content.component';
import { AngularComponent } from './content/angular/angular.component';
import { HtmlCssComponent } from './content/html-css/html-css.component';
import { JsComponent } from './content/js/js.component';
import { ProfessionalComponent } from './content/professional/professional.component';
import { NodejsComponent } from './content/nodejs/nodejs.component';

const appRoutes: Routes = [
  { path: '', redirectTo: '/cover', pathMatch: 'full' },
  { path: 'cover', component: CoverComponent },
  { path: 'content', component: ContentComponent, children: [
    { path: 'angular', component: AngularComponent },
    { path: 'html-css', component: HtmlCssComponent },
    { path: 'js', component: JsComponent },
    { path: 'professional', component: ProfessionalComponent },
    { path: 'nodejs', component: NodejsComponent }
  ]}
]

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
    
}