import { Component, OnInit } from '@angular/core';
import { NavService } from '../../services/nav-service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  navActive = false;

  constructor(private navService: NavService) { }

  ngOnInit() {
    this.navService.navStatusChanged.subscribe((status:boolean) => {
      this.navActive = status;
    });
  }

}
