import { Component, OnInit } from '@angular/core';
import { NavService } from '../../services/nav-service.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  navActive = false;

  constructor(private navService: NavService) { }

  ngOnInit() {
    this.navService.navStatusChanged.subscribe((status) => {
      this.navActive = status;
    });
  }

}
