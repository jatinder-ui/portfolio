import { Component, OnInit, HostBinding } from '@angular/core';
import { ActivatedRoute, Router, Params, NavigationStart } from "@angular/router";
import 'rxjs/add/operator/filter';
import { NavService } from '../services/nav-service.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
  navActive = false;

  @HostBinding('class.nav--slideLeft') navSlideLeft: boolean = false;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private navService: NavService) { 
                router.events
                  .filter(event => event instanceof NavigationStart)
                  .subscribe((event:NavigationStart) => {
                      if (event.url !== "/" && event.url !== "/cover") {
                        this.navSlideLeft = true;
                      } else {
                        this.navSlideLeft = false;
                      }
                      
                      this.navActive = this.navSlideLeft;
                      this.navService.setNavStatus(!this.navSlideLeft);
                  });
              }

  ngOnInit() {
  }

}
