import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class NavService {
  private navActive = false;
  navStatusChanged = new Subject<boolean>();

  constructor() { }

  getNavStatus() {
    return this.navActive;
  }

  setNavStatus(status: boolean) {
    this.navActive = status;
    this.navStatusChanged.next(this.navActive);
  }

}
