import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { HeaderComponent } from './nav/header/header.component';
import { ContactComponent } from './nav/contact/contact.component';
import { CoverComponent } from './nav/cover/cover.component';
import { NavComponent } from './nav/nav.component';
import { ContentComponent } from './content/content.component';
import { AppRoutingModule } from './app-routing.module';
import { NavService } from './services/nav-service.service';
import { DemoListComponent } from './nav/demo-list/demo-list.component';
import { AngularComponent } from './content/angular/angular.component';
import { HtmlCssComponent } from './content/html-css/html-css.component';
import { JsComponent } from './content/js/js.component';
import { ProfessionalComponent } from './content/professional/professional.component';
import { NodejsComponent } from './content/nodejs/nodejs.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ContactComponent,
    CoverComponent,
    NavComponent,
    ContentComponent,
    DemoListComponent,
    AngularComponent,
    HtmlCssComponent,
    JsComponent,
    ProfessionalComponent,
    NodejsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [NavService],
  bootstrap: [AppComponent]
})
export class AppModule { }
